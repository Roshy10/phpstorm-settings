<?php
#parse("Codeigniter.php")

#if (${NAMESPACE})

namespace ${NAMESPACE};

#end

class ${NAME} extends CI_Controller
{
public function index()
	{
	    /** @noinspection PhpUndefinedFieldInspection */
		$data['site_name'] = $this->config->item('site_name');
		$this->load->view('header', $data);
		$this->load->view('${NAME}');
		$this->load->view('footer');
	}
}